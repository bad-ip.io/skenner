<?php

set_time_limit(0);
header( 'Content-type: text/html; charset=utf-8' );
ob_start();

class skenner {

    public $infected_files = array();
    private $scanned_files = array();
    private $hidden_files = array();
    private $params = null;
    private $reportEmailAddress = 'bad_ip@iridiumintel.com';
    private $targetExt = null;
    private $maxFileSize = 100 * 1024 * 1024 /* 100MB */;

    private $startTs, $endTs, $finalTs;
    private $scannedCount = 0, $scannedDirCount = 0, $evilCount = 0, $hiddenCount = 0;

    private $logging = false;
    private $box = false;
    private $hidden = false;
    private $sanitize = false;
    private $hiddenNinja = [
        '.appveyor.yml'
        ,'.auto-changelog'
        ,'.babelrc'
        ,'.babelrc.js'
        ,'.babelrc.test.js'
        ,'.bash_history'
        ,'.beautifyrc'
        ,'.bower.json'
        ,'.bowerrc'
        ,'.codeclimate.yml'
        ,'.composer-auth.json'
        ,'.coveralls.yml'
        ,'.cspell.json'
        ,'.csscomb.json'
        ,'.csslintrc'
        ,'.dir-locals.el'
        ,'.dockerignore'
        ,'.doctrine-project.json'
        ,'.DS_Store'
        ,'.editorconfig'
        ,'.env'
        ,'.env.example'
        ,'.env.sample'
        ,'.eslintignore'
        ,'.eslintrc'
        ,'.eslintrc.js'
        ,'.eslintrc.json'
        ,'.eslintrc.yaml'
        ,'.eslintrc.yml'
        ,'.gitattributes'
        ,'.gitignore'
        ,'.gitkeep'
        ,'.gitlab-ci.yml'
        ,'.gitmodules'
        ,'.gitstats.yml'
        ,'.gush.yml'
        ,'.htaccess'
        ,'.htmllintrc'
        ,'.index.js.un~'
        ,'.ini.php'
        ,'.istanbul.yml'
        ,'.jms.yml'
        ,'.jscs.json'
        ,'.jscsrc'
        ,'.jshintignore'
        ,'.jshintrc'
        ,'.lint'
        ,'.litstrings'
        ,'.multi-tester.yml'
        ,'.npmignore'
        ,'.nvmrc'
        ,'.package.json.un~'
        ,'.release.json'
        ,'.scrutinizer.yml'
        ,'.scss-lint.yml'
        ,'skenner.php'
        ,'.stickler.yml'
        ,'.styleci.yml'
        ,'.stylelintignore'
        ,'.stylelintrc'
        ,'.stylelintrc.json'
        ,'.svgo.yml'
        ,'.tangle.ini.php'
        ,'.testem.json'
        ,'.travis.yml'
        ,'.webtree.ini.php'
        ,'.zuul.yml'
    ];

    public $filenameReport = null;
    public $filenameFiles = null;

    private $evilTerms = [
        'bajatax', 'wp_ajax_try_2020_v2', 'file_get_contents("https://api.telegram.org/bot',
        'gzinflate(base64_decode',
        '$GLOBALS[\'',
        'alexusMailer', 'md5($_HEADERS', '@include "\0', 'echo @serialize',
        'base64_decode(', 'base64_decode (', 'error_reporting(0)', 'shell_exec(', 'shell_exec ('
    ];

    private $evilExt = [
        '*.suspected', '.*.ico'
    ];

    private $evilNick = [
        '.quarantine', '.tmb'
    ];

    function __construct() {
        $this->init();
        $this->scan(dirname(__FILE__));
        $this->sendalert();
    }

    /**
     * @param $dir
     * @throws Exception
     */
    function scan($dir) {
        $this->startTs = microtime();
        $this->scanned_files[] = $dir;
        $files = scandir($dir);

        if(!is_array($files)) {
            throw new Exception('Unable to scan directory ' . $dir . '.  Please make sure proper permissions have been set.');
        }

        $this->scannedDirCount++;

        if (!in_array($dir, $this->infected_files) && !in_array($dir, $this->hiddenNinja)) {
            foreach ($this->evilNick as $name) {
                if ($name == $dir) {
                    $this->evilCount++;
                    $this->logging ? file_put_contents('skenner/'.$this->filenameReport.'/'.$this->filenameFiles, $dir."\r", FILE_APPEND) : null;
                    $this->infected_files[] = $dir;
                    if ($this->sanitize) {
                        @self::recursiveDelete($dir);
                        self::output("rm+".$dir."+");
                    }
                }
            }
        }


        if ($this->hidden && $dir[0] == '.' && !in_array($dir, $this->hidden_files) && !in_array($dir, $this->hiddenNinja)) { //hidden
            $this->hiddenCount++;
            $this->hidden_files[] = $dir;
            self::output("d+".$dir."+");
            $this->logging ? file_put_contents('skenner/'.$this->filenameReport.'/'.$this->filenameFiles.'-hidden', $dir."\r\n", FILE_APPEND) : null;
        }


        foreach($files as $file) {
            if(is_file($dir.'/'.$file) && !in_array($dir.'/'.$file, $this->scanned_files) &&
                strpos($dir, 'skenner') === false && strpos($file, basename(__FILE__, '.php')) === false) {
                self::output(".");
                $this->scannedCount++;

                if (!in_array(pathinfo($file)['basename'], $this->infected_files) && !in_array(pathinfo($file)['basename'], $this->hiddenNinja)) {
                    foreach ($this->evilNick as $name) {
                        if ($name == pathinfo($file)['basename']) {
                            $this->evilCount++;
                            $this->logging ? file_put_contents('skenner/'.$this->filenameReport.'/'.$this->filenameFiles, pathinfo($file)['basename']."\r", FILE_APPEND) : null;
                            $this->infected_files[] = $dir;
                        }
                    }
                }

                if ($this->hidden && isset(pathinfo($file)['basename']) && pathinfo($file)['basename'][0] == '.' && //hidden
                    !in_array($file, $this->hidden_files) && !in_array(pathinfo($file)['basename'], $this->hiddenNinja)) {

                    $this->hiddenCount++;
                    $this->hidden_files[] = $dir.'/'.$file;
                    $this->logging ? file_put_contents('skenner/'.$this->filenameReport.'/'.$this->filenameFiles.'-hidden', $dir.'/'.$file."\r", FILE_APPEND) : null;
                    $this->box ? $this->boxIt($dir.'/'.$file) : null;
                    self::output("f+".pathinfo($file)['basename']."+");

                }

                filesize($dir.'/'.$file) <= $this->maxFileSize ? $this->check(file_get_contents($dir.'/'.$file),$dir.'/'.$file) : null;
            } elseif(is_dir($dir.'/'.$file) && substr($file,0,1) != '.') {

                $this->scan($dir.'/'.$file);
            }
        }
    }

    function boxIt($file) {
        copy($file, 'skenner/'.$this->filenameReport.'/box/'.pathinfo($file)['basename']);
    }

    function check($contents, $file) {
//        /eval\((base64|eval|\$_|\$[A-Za-z_0-9{]*([({\[]))/i
        if (!is_null($this->targetExt) && isset(pathinfo($file)['extension']) && pathinfo($file)['extension'] != $this->targetExt) return;
        $positive = false;
        $this->scanned_files[] = $file;

        !$positive && preg_match('/eval\((base64|eval|\$_|\$\$|\$[A-Za-z_0-9\{]*(\(|\{|\[))/i', $contents) ? $positive = true : null;

        if (!$positive) {
            foreach ($this->evilTerms as $term) {
                strpos($contents, $term) != false ? $positive = true : null;
            }
        }

        if($positive) {
            $this->evilCount++;
            self::output("+".pathinfo($file)['basename']."+");
            $this->logging ? file_put_contents('skenner/'.$this->filenameReport.'/'.$this->filenameFiles, $file."\r", FILE_APPEND) : null;
            $this->box ? $this->boxIt($file) : null;
            $this->infected_files[] = $file;
        }
    }


    function sendalert() {
        $this->endTs = microtime();
        if(count($this->infected_files) != 0) {
            @$creationTime = number_format((float)(($this->endTs - $this->startTs) / 1000), 5, '.', '');

            $message = "== SKENNER LOG /".date('d-m-Y HH:mm')." == \n";
            $message .= "Total files: $this->scannedCount  Total dirs: $this->scannedDirCount  Possibly evil: $this->evilCount   Hidden: $this->hiddenCount   Total time: $creationTime seconds\n\n";
            $message .= "LIST OF SUSPICIOUS FILES: \n";
            foreach($this->infected_files as $inf) {
                $message .= "  -  $inf \n";
            }
            if (!empty($this->hidden_files)) {
                $message .= "\nLIST OF HIDDEN FILES AND FOLDERS: \n";
                foreach($this->hidden_files as $inf) {
                    $message .= "  -  $inf \n";
                }
            }
            $this->logging ? file_put_contents('skenner/'.$this->filenameReport.'/'.$this->filenameReport, $message) : null;
            mail($this->reportEmailAddress,'SKENNER REPORT',$message,'FROM:');
        }
    }


    function init() {

        if (defined('STDIN') && isset($_SERVER['argv']) && count($_SERVER['argv']) > 0) {
            $array = [];
            unset($_SERVER['argv'][0]);
            foreach ($_SERVER['argv'] as $arg) {
                $e = explode("=", $arg);
                if(count($e) == 2)
                    $array[$e[0]] = $e[1];
            }
            $this->params = $array;
        } else if (isset($_GET) && !empty($_GET)) {
            $this->params = $_GET;
        }

        $this->filenameReport = 'scan-'.date('m-d-Y_hia');
        $this->filenameFiles = 'scan-files-'.date('m-d-Y_hia');

        if (is_null($this->params) || empty($this->params)) {
            $str = <<<EOD

No options have been set

spanning multiple lines
using heredoc syntax.

emailto=email@address.com | log=1 | box=1 | hidden=1 | target_ext=php | sanitize=1

EOD;
            self::output($str);
            die(0);
        }

        self::output("Scanning.... \n\r");

        foreach($this->params as $key => $value) {

            switch ($key) {
                case 'emailto':
                    self::output(" * Setting custom email address for reporting <$value>\n");
                    $this->reportEmailAddress = $value;
                    break;
                case 'log':
                    self::output(" * Enabling logs\n");
                    $this->logging = true;
                    !file_exists('skenner/'.$this->filenameReport) ? mkdir('skenner/'.$this->filenameReport, 0755, true) : null;
                    break;
                case 'box':
                    self::output(" * Enabling box. Storing to skenner/$this->filenameReport/box\n");
                    $this->box = true;
                    !file_exists('skenner/'.$this->filenameReport.'/box') ? mkdir('skenner/'.$this->filenameReport.'/box', 0755, true) : null;
                    break;
                case 'hidden':
                    self::output(" * Enabling hidden files monitor\n");
                    $this->hidden = true;
                    !file_exists('skenner/'.$this->filenameReport) ? mkdir('skenner/'.$this->filenameReport, 0755, true) : null;
                    break;
                case 'target_ext':
                    self::output(" * Enabling targeting file extension $value\n");
                    $this->targetExt = $value;
                    break;
                case 'sanitize':
                    self::output(" * Enabling sanitize\n");
                    $this->sanitize = $value;
                    break;

            }

        }




    }


    function recursiveDelete($str) {
        if (is_file($str)) {
            return @unlink($str);
        } elseif (is_dir($str)) {
            $scan = glob(rtrim($str, '/').'/*');
            foreach ($scan as $index => $path) {
                self::recursiveDelete($path);
            }
            return @rmdir($str);
        }
        return false;
    }

    function output($str, $end = false) {
        echo $str;
        $end ? ob_end_flush() : ob_flush();
    }


}



ini_set('memory_limit', '-1');

$skenner = new skenner;

$skenner->output("\n\rits done " . date('Y-m-d H:i:s') . "\n\r", true);